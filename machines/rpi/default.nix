{ config, lib, pkgs, ... }:
{
  nixpkgs.system = "aarch64-linux";

  hardware = {
    enableRedistributableFirmware = true;
    firmware = [
      pkgs.wireless-regdb
      pkgs.raspberrypifw
      pkgs.raspberrypiWirelessFirmware
    ];
  };

  networking.wireless.enable = true;

  #boot.kernelPackages = pkgs.linuxPackages_latest;

  boot = {
    consoleLogLevel = lib.mkDefault 7;
    kernelParams = ["cma=32M" "console=ttyS1,115200n8" "console=tty0"];
    extraModprobeConfig = ''
      options cf680211 ieee80211_regdom="DE"
    '';
    loader = {
      grub.enable = false;
      generic-extlinux-compatible.enable = true;
      raspberryPi = {
        enable = true;
        version = 3;
        uboot.enable = false;
      };
    };
  };

  fileSystems = {
    "/" = {
      device = "/dev/disk/by-label/NIXOS_SD";
      fsType = "ext4";
    };
  };

  #swapDevices = [ { device = "/swapfile"; size=1024; } ];
}
