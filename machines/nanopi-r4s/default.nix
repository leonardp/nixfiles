{ config, ... }:
{
  imports = [
    ./boot.nix
    ./led.nix
    ./udev-rules.nix
  ];

  nixpkgs.system = "aarch64-linux";

  fileSystems = {
    "/" = {
      device = "/dev/disk/by-label/NIXOS_SD";
      fsType = "ext4";
    };
  };

  #swapDevices = [ { device = "/swapfile"; size=1024; } ];

}
