{
  networking.usePredictableInterfaceNames = false;
  #services.udev.initrdRules = ''
  #  KERNEL=="enp1s0", NAME="lan"
  #'';
  services.udev.extraRules = ''
    DRIVERS=="rk_gmac-dwmac", NAME="wan"
    DRIVERS=="r8169", NAME="lan"
  '';
}
