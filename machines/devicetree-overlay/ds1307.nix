{
  hardware.deviceTree.overlays = [
    { name = "ds1307";
      dtsText = ''
      /dts-v1/;
      /plugin/;

      / {
      compatible = "raspberrypi";

        fragment@0 {
          target = <&i2c1>;
          __overlay__ {
            status = "okay";

            target = <0x01>;
            #address-cells = <0x01>;
            #size-cells = <0x00>;

            ds1307@68 {
              compatible = "dallas,ds1307";
              reg = <0x68>;
              phandle = <0x04>;
            };
          };
        };
      };
      '';
    }
  ];
}
