{ config, pkgs, ... }:
{
  # Boot loader.
  boot.loader.grub.enable = true;
  boot.loader.grub.version = 2;
  boot.loader.grub.device = "/dev/vda";

  # Networking.
  networking.useDHCP = true;
  networking.firewall.enable = false;
}
