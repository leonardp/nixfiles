{ config, lib, pkgs, ... }:
{
  #boot.isContainer = true;
  #networking.useDHCP = lib.mkForce true;
  networking.firewall.enable = false;

  services = {
    #openssh.enable = true;
  };

  users.users.ggm = {
    isNormalUser = true;
    #extraGroups = [ "wheel" ];
    #password = "ggm";
  };

  # disable documentation to speed up build
  documentation.enable = false;
}
