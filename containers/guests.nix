{
  yii_webstack = {
    desc = "nginx + php-fpm for yii apps";
    name = "phpdev";
    ip = "192.168.100.10";
  };

  yii_mysql = {
    desc = "mysql database for yii apps";
    name = "yii-mysql";
    ip = "192.168.100.11";
  };
}
