{ config, pkgs, lib, ... }:
let
  pubkeys = import ../pubkeys.nix;
in
{
  users.extraUsers.leo = {
    isNormalUser = true;
    extraGroups = lib.mkDefault [ "wheel" "networkmanager" "audio" "video" "lp" "dialout" "libvirtd" "kvm" "nm-openvpn" "lxd" "disk" "scanner" "usb" "docker" "uucp" "users" ];
    openssh.authorizedKeys.keys = lib.mkDefault [
      pubkeys.leo_p
      pubkeys.leo_midas
    ];
    initialPassword = "changepw";
  };
}
