{
  leo_p = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIPc5dear8nQf3y1iUzTHibOlJDtdQjcxQXF28d8hyssF leo@private";
  leo_midas = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIN+4ccf39xHj5sF+jGX/qv/kpHbpffCFxyDJcQwXZ97O leonardp@midas";
  leo_bin_cache = "adelskronen.lan:cnItGgqOm2Bg8AznUlnV7JjE45z5cn8KvOL0uY6kQtU=";
  pluto = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAINJK45S80HPFsmb6qxcxUrNTQ7e1OxRNlqaxxj42yfxA root@pluto";
}
