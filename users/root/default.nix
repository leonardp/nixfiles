{ config, pkgs, lib, ... }:
let
  pubkeys = import ../pubkeys.nix;
in
{
  users.users.root = {
    openssh.authorizedKeys.keys = lib.mkDefault [
      pubkeys.leo_p
      pubkeys.leo_midas
    ];
    #initialPassword = "changepw";
  };
}
