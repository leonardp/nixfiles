{
  services.resolved = {
    enable = true;
    domains = [ "lan" "slowfi" "nanolan" "ipc.uni-tuebingen.de" ];
    llmnr = "false";
    fallbackDns = [ "84.200.69.80" "84.200.70.40" ]; # DNS.WATCH
    #fallbackDns = [ "8.8.8.8" "8.8.4.4" ];

    # workaround for https://github.com/NixOS/nixpkgs/issues/66451
    dnssec = "false";

    extraConfig = ''
      # might help if there is a local nameserver instance
      DNSStubListener=yes
      #DNSOverTLS=opportunistic # *good* for captive portals
      Cache=no
      #MulticastDNS=no
      #ReadEtcHosts=yes
    '';
  };
}
