{ config, pkgs, ... }:
{
  # ovveride pattern for path from git
  #nixpkgs.config.packageOverrides = super: let self = super.pkgs; in {
  #  lxc = super.lxc.overrideAttrs (oldAttrs: rec {
  #    patches = oldAttrs.patches ++ [
  #      (self.fetchpatch {
  #        url = "https://github.com/lxc/lxc/commit/b31d62b847a3ee013613795094cce4acc12345ef.patch";
  #        sha256 = "1jpskr58ih56dakp3hg2yhxgvmn5qidi1vzxw0nak9afbx1yy9d4";
  #      })
  #    ];
  #  });
  #};

  virtualisation.lxd = {
    #enable = true;
    enable = false;
    zfsSupport = false;
  };

  # add optional network config here?
  # networking.bridges.lxdbr0 = {
  #   interfaces = [ ];
  # };
  #
  #networking.iproute2.rttablesExtraConfig = "
  #  ip -4 route add 192.168.0.0/24 via 192.168.0.1
  #";

  # something strange...
  #security.apparmor.enable = false;
}
