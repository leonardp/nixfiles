let
  pinnedNixpkgs = let
    channelRelease =
      "nixos-19.09pre190687.3f4144c30a6"; # last known working mongo
    channelName = "unstable";
    url =
      "https://releases.nixos.org/nixos/${channelName}/${channelRelease}/nixexprs.tar.xz";
    sha256 = "040f16afph387s0a4cc476q3j0z8ik2p5bjyg9w2kkahss1d0pzm";
  in import (builtins.fetchTarball { inherit url sha256; }) {
    system = pkgs.system; # TODO: ? is this the best way?
    # TODO: inherit overlays too?
  };
  mdbp = pinnedNixpkgs.mongodb;
in {
  # both UniFi and Oracle's JRE are closed-source, we allow that here:
  nixpkgs.config = {
    allowUnfree = true;
    oraclejdk.accept_license = true;
  };

  # enable the main service!
  services.unifi = {
    enable = true;

    #dataDir = "/usbstick";

    # can't restore from backups made from newer versions
    # so we use a newer version also
    unifiPackage = pkgs.unifiStable;

    jrePackage = pkgs.jre8_headless;

    #mongodbPackage = pkgs.mongodb-3_6;
    mongodbPackage = mdbp;
  };
}
