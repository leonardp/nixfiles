{
  # Resizefs
  systemd.services.resize-main-fs = {
    wantedBy = [ "multi-user.target" ];
    serviceConfig.Type = "oneshot";
    script =
      ''
        # Resize main partition to fill whole disk
        echo ", +" | ${pkgs.utillinux}/bin/sfdisk /dev/vda --no-reread -N 1
        ${pkgs.parted}/bin/partprobe
        # Resize filesystem
        ${pkgs.e2fsprogs}/bin/resize2fs /dev/vda1
      '';
  };
}
