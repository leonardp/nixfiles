{ config, lib, pkgs, ...}:


let

  inherit (lib)
    types
    mkEnableOption
    mkOption
    mkIf
  ;

  cfg = config.services.nanomq;

  boolToConfigString = b: if b then "yes" else "no";

  package = cfg.package;

  nanomqConf = pkgs.writeText "nanomq.conf" ''
    daemon=no

    url=tcp://${toString cfg.tcpHost}:${toString cfg.tcpPort}

    num_taskq_thread=${toString cfg.numTaskqThread}
    max_taskq_thread=${toString cfg.maxTaskqThread}
    parallel=${toString cfg.parallel}
    property_size=${toString cfg.propertySize}
    msq_len=${toString cfg.msqLength}
    qos_duration=${toString cfg.qosDuration}
    allow_anonymous=${boolToConfigString cfg.allowAnonymous}
    websocket.enable=${boolToConfigString cfg.wsEnable}
    websocket.url=ws://${toString cfg.wsHost}:${toString cfg.wsPort}/mqtt
    http_server.enable=${boolToConfigString cfg.httpEnable}
    http_server.port=${toString cfg.httpPort}
    http_server.username=${toString cfg.httpUser}
    http_server.password=${toString cfg.httpPassword}
  '';

in

{

  ###### Interface

  options = {
    services.nanomq = {

      enable = mkEnableOption "Enable the Nanomq MQTT broker";

      package = mkOption {
        default = pkgs.nanomq;
        type = types.package;
        description = ''
          Nanomq package to use.
        '';
      };

      tcpHost = mkOption {
        default = "127.0.0.1";
        type = types.str;
        description = ''
          TCP Host to listen on.
        '';
      };

      tcpPort = mkOption {
        default = 1883;
        type = types.int;
        description = ''
          TCP Port on which to listen.
        '';
      };

      wsEnable = mkOption {
        default = true;
        type = types.bool;
        description = ''
          Enable WS Host to listen on.
        '';
      };

      wsHost = mkOption {
        default = "127.0.0.1";
        type = types.str;
        description = ''
          WS Host to listen on.
        '';
      };

      wsPort = mkOption {
        default = 8083;
        type = types.int;
        description = ''
          WS Port on which to listen.
        '';
      };

      httpEnable = mkOption {
        default = true;
        type = types.bool;
        description = ''
          Enable HTTP Host to listen on.
        '';
      };

      httpHost = mkOption {
        default = "127.0.0.1";
        type = types.str;
        description = ''
          TCP Host to listen on.
        '';
      };

      httpPort = mkOption {
        default = 8081;
        type = types.int;
        description = ''
          TCP Port on which to listen.
        '';
      };

      httpUser = mkOption {
        default = "admin";
        type = types.str;
        description = ''
          HTTP server username.
        '';
      };

      httpPassword = mkOption {
        default = "public";
        type = types.str;
        description = ''
          HTTP server password.
        '';
      };

      numTaskqThread = mkOption {
        default = 4;
        type = types.ints.between 1 255;
        description = ''
          Use a specified number of taskq threads.
        '';
      };

      maxTaskqThread = mkOption {
        default = 4;
        type = types.ints.between 1 255;
        description = ''
          Use a specified maximunm number of taskq threads.
        '';
      };

      parallel = mkOption {
        default = 32;
        type = types.ints.unsigned;
        description = ''
          Handle a specified maximum number of outstanding requests.
        '';
      };

      propertySize = mkOption {
        default = 32;
        type = types.ints.unsigned;
        description = ''
          The max size for a MQTT user property.
        '';
      };

      msqLength = mkOption {
        default = 64;
        type = types.ints.unsigned;
        description = ''
          The queue length for resending messages.
        '';
      };

      qosDuration = mkOption {
        default = 360;
        type = types.ints.unsigned;
        description = ''
          The nano qos duration.
        '';
      };

      allowAnonymous = mkOption {
        default = true;
        type = types.bool;
        description = ''
          Allow anonymous login.
        '';
      };

      dataDir = mkOption {
        default = "/opt/nanomq";
        type = types.path;
        description = ''
          The data directory.
        '';
      };

    };
  };


  ###### Implementation

  config = mkIf cfg.enable {

    systemd.services.nanomq = {
      description = "Nanomq MQTT Broker Daemon";
      wantedBy = [ "multi-user.target" ];
      after = [ "network.target" ];
      serviceConfig = {
        Type = "notify";
        NotifyAccess = "main";
        User = "nanomq";
        Group = "nanomq";
        RuntimeDirectory = "nanomq";
        WorkingDirectory = cfg.dataDir;
        Restart = "on-failure";
        ExecStart = "${package}/bin/nanomq broker start -conf ${nanomqConf}";
        ExecReload = "${package}/bin/nanomq broker restart"; # TODO: reload vs. restart?!
        #ExecReload = "${pkgs.coreutils}/bin/kill -HUP $MAINPID";
        #ExecStop = "${package}/bin/nanomq broker stop"; # TODO: rlly?

        # Hardening
        CapabilityBoundingSet = "";
        DevicePolicy = "closed";
        LockPersonality = true;
        MemoryDenyWriteExecute = true;
        NoNewPrivileges = true;
        PrivateDevices = true;
        PrivateTmp = true;
        PrivateUsers = true;
        ProtectClock = true;
        ProtectControlGroups = true;
        ProtectHome = true;
        ProtectHostname = true;
        ProtectKernelLogs = true;
        ProtectKernelModules = true;
        ProtectKernelTunables = true;
        ProtectProc = "invisible";
        ProcSubset = "pid";
        ProtectSystem = "strict";
        ReadWritePaths = [
          cfg.dataDir
          "/tmp"  # TODO: testme! needed for nng sockets?
        ];
        RemoveIPC = true; # TODO: testme! needed for POSIX queue?!
        RestrictAddressFamilies = [
          "AF_UNIX"  # for sd_notify() call??
          "AF_INET"
          "AF_INET6"
        ];
        RestrictNamespaces = true;
        RestrictRealtime = true;
        RestrictSUIDSGID = true;
        SystemCallArchitectures = "native";
        SystemCallFilter = [
          "@system-service"
          "~@privileged"
          "~@resources"
        ];
        UMask = "0077";
      };
    };

    users.users.nanomq = {
      description = "Nanomq MQTT Broker Daemon owner";
      group = "nanomq";
      isSystemUser = true;
      home = cfg.dataDir;
      createHome = true;
    };
    users.groups.nanomq.members = [ "nanomq" ];

  };
}
