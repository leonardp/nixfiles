{ pkgs, lib, ... }:
{
  services.postgresql = {
    enable = true;
    package = pkgs.postgresql_14;
    extraPlugins = [
      pkgs.postgresql14Packages.timescaledb
      pkgs.postgresql14Packages.postgis
    ];
    settings = {
      shared_preload_libraries = "timescaledb";
    };
    # TODO howto password/secret management for production
    initialScript = pkgs.writeText "backend-initScript" ''
      CREATE DATABASE gilgamesh;
      CREATE DATABASE geodata;

      CREATE ROLE gilgamesh WITH LOGIN PASSWORD 'gilgamesh' CREATEDB;
      GRANT ALL PRIVILEGES ON DATABASE gilgamesh TO gilgamesh;

      CREATE ROLE geodata WITH LOGIN PASSWORD 'geodata' CREATEDB;
      GRANT ALL PRIVILEGES ON DATABASE geodata TO geodata;

      \connect gilgamesh postgres
      CREATE EXTENSION timescaledb;

      \connect geodata postgres
      CREATE EXTENSION postgis;
      CREATE EXTENSION postgis_raster;
      CREATE EXTENSION postgis_topology;
      CREATE EXTENSION postgis_sfcgal;
      CREATE EXTENSION fuzzystrmatch;
      CREATE EXTENSION address_standardizer;
      CREATE EXTENSION address_standardizer_data_us;
      CREATE EXTENSION postgis_tiger_geocoder;
    '';
    enableTCPIP = true;
    authentication = lib.mkForce ''
      local all all              peer
      host  all all 127.0.0.1/32 md5
      host  all all ::1/128      md5
      host  all all 0.0.0.0/0    md5
      host  all all ::/0         md5
    '';

  };
  users.users.postgres.initialPassword = "postgres";

  imports = [
    ./phppgadmin.nix
  ];
  services.phppgadmin = {
    enable = true;
    phpPackage = pkgs.php74;
    virtualHost = {
      listen = [
        { addr = "0.0.0.0"; port = 9090; }
      ];
    };
    settings = {
      theme = "cappuccino";
      left_width = 300;
      extra_login_security = false;
    };
    dbHosts = [
      { desc = "LOCAL"; }
      {
        desc = "LOCAL_ip";
        host = "127.0.0.1";
      }
    ];
  };
}
