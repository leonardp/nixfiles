{ config, pkgs, ... }:
{
  virtualisation.docker = {
    enable = true;
    extraOptions = "--dns=8.8.8.8";
  };

  # add optional network config here?
  # networking.bridges.lxdbr0 = {
  #   interfaces = [ ];
  # };
  #
  #networking.iproute2.rttablesExtraConfig = "
  #  ip -4 route add 192.168.0.0/24 via 192.168.0.1
  #";

  # something strange...
  #security.apparmor.enable = false;
}
