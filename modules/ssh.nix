{
  networking.firewall.allowedTCPPorts = [ 22 ];
  services.openssh = {
    enable = true;
    # Only pubkey auth
    passwordAuthentication = false;
    challengeResponseAuthentication = false;
  };
}
