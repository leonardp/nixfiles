# https://nixos.wiki/wiki/Overlays
# https://gitlab.com/samueldr/nixos-configuration/-/blob/50b1c17f3f2668b18bf83edaf67fb44e9a438ba4/modules/overlays-compat/overlays.nix
self: super:
with super.lib;
let
  # Using the nixos plumbing that's used to evaluate the config...
  eval = import <nixpkgs/nixos/lib/eval-config.nix>;
  # Evaluate the config,
  paths = (eval {modules = [(import <nixos-config>)];})
    # then get the `nixpkgs.overlays` option.
    .config.nixpkgs.overlays
  ;
in
foldl' (flip extends) (_: super) paths self
