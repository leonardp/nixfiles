{ config, pkgs, lib, ... }:
let
  mywifi = import ../../secrets/wifi.nix;
in
{
  imports = [
    ./baseline.nix
    /etc/nixos/machines/rpi
    #./rpi-boot.nix #TODO merge with machines/rpi.....
    <nixpkgs/nixos/modules/installer/sd-card/sd-image-aarch64.nix>
  ];

  # compression takes loads of time with emulation, skip it.
  sdImage.compressImage = false;

  networking.wireless = {
    enable = true;
    networks = mywifi;
    interfaces = [ "wlan0" ];
  };
}
