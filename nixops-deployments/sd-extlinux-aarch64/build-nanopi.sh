#! /usr/bin/env bash
set -euo pipefail
. imgbuilder.sh

build_image "nanopi-r4s"

nix-build '<nixos/nixpkgs>' --argstr system aarch64-linux -A uboot_NanopiR4S
cp result/* $OUT/

dd if=$OUT/idbloader.img of=$OUT/$IMG conv=fsync,notrunc bs=512 seek=64
dd if=$OUT/u-boot.itb of=$OUT/$IMG conv=fsync,notrunc bs=512 seek=16384


if [ $# -ge 1 ] && [ -n "$1" ] && [ $1 == "secret" ]
then
	echo "Provisioning secret..."
	copy_secret $OUT/$IMG $2
else
	echo "Not provisioning secret..."
fi

print_success $OUT $IMG
