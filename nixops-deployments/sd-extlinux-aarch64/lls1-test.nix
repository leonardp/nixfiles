{ config, lib, pkgs, ... }:
let
  mywifi = import ../../secrets/wifi.nix;
in
{
  imports = [
    <nixpkgs/nixos/modules/installer/cd-dvd/sd-image.nix>
    imgstuff/boot.nix
    imgstuff/minify.nix
    ../../profiles/users/leo/user.nix
    ../../profiles/users/root/user.nix
    ../../profiles/nvim.nix
    ../../modules/ssh.nix
    ../../modules/ntp.nix
    ../../modules/resolved.nix
    ../../profiles/lls1-test.nix
  ];

  # building with emulation
  nixpkgs.system = "aarch64-linux";
  # bzip2 compression takes loads of time with emulation, skip it.
  sdImage.compressImage = false;

  networking.firewall.enable = false;

  networking.wireless = {
    enable = true;
    networks = mywifi;
  };

}
