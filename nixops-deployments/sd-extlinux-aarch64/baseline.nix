{ config, lib, pkgs, ... }:
let
  name = "devpi";
  #name = "provision-nixops";
in
{
  imports = [
    ./minify.nix
    #/etc/nixos/profiles/provision-nixops.nix
    /etc/nixos/hosts/devpi
  ];

  networking.hostName = name;

  #services.getty.autologinUser = lib.mkDefault "root";
  #users.extraUsers.root.openssh.authorizedKeys.keys = [
  #   ""
  #];
}
