{ config, pkgs, lib, ... }:
let
  #extlinux-conf-builder =
  #  import <nixpkgs/nixos/modules/system/boot/loader/generic-extlinux-compatible/extlinux-conf-builder.nix> {
  #    pkgs = pkgs.buildPackages;
  #  };
in {
  imports = [
    #<nixpkgs/nixos/modules/installer/cd-dvd/sd-image.nix>
    <nixpkgs/nixos/modules/installer/sd-card/sd-image-aarch64.nix>
  ];

  boot.loader = {
    grub.enable = false;
    generic-extlinux-compatible.enable = true;
    raspberryPi = {
      enable = true;
      version = 3;
      uboot.enable = false;
      firmwareConfig = ''
        # 'fixes' serial console
        dtoverlay=disable-bt

        dtparam=i2c_arm=on
        dtoverlay=rtc-i2c,ds1307
      '';
    };
  };

  #boot.kernelPackages = pkgs.linuxPackages_rpi3;
  #boot.kernelPackages = pkgs.linuxPackages_latest;

  #sdImage = {
  #  populateFirmwareCommands = let
  #    configTxt = pkgs.writeText "config.txt" ''
  #      kernel=u-boot-rpi3.bin

  #      # Boot in 64-bit mode.
  #      arm_control=0x200

  #      # U-Boot used to need this to work, regardless of whether UART is actually used or not.
  #      # TODO: check when/if this can be removed.
  #      enable_uart=1

  #      # Prevent the firmware from smashing the framebuffer setup done by the mainline kernel
  #      # when attempting to show low-voltage or overtemperature warnings.
  #      avoid_warnings=1

  #      # 'fixes' serial console
  #      dtoverlay=disable-bt
  #    '';
  #    in ''
  #      (cd ${pkgs.raspberrypifw}/share/raspberrypi/boot && cp bootcode.bin fixup*.dat start*.elf $NIX_BUILD_TOP/firmware/)
  #      cp ${pkgs.ubootRaspberryPi3_64bit}/u-boot.bin firmware/u-boot-rpi3.bin
  #      cp ${configTxt} firmware/config.txt
  #    '';
  #  populateRootCommands = ''
  #    mkdir -p ./files/boot
  #    ${extlinux-conf-builder} -t 3 -c ${config.system.build.toplevel} -d ./files/boot
  #  '';
  #};
}
