#!/usr/bin/env bash
set -euo pipefail
. imgbuilder.sh


build_image "rpi3"

if [ $# -ge 1 ] && [ -n "$1" ] && [ $1 == "secret" ]
then
	echo "Provisioning secret..."
	copy_secret $OUT/$IMG $2
else
	echo "Not provisioning secret..."
fi

print_success $OUT $IMG
