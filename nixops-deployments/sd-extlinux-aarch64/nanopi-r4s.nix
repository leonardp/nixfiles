{ config, pkgs, lib, ... }:
let
  extlinux-conf-builder =
    import <nixpkgs/nixos/modules/system/boot/loader/generic-extlinux-compatible/extlinux-conf-builder.nix> {
      pkgs = pkgs.buildPackages;
    };
in {
  imports = [
    ./baseline.nix
    /etc/nixos/machines/nanopi-r4s
    <nixpkgs/nixos/modules/installer/sd-card/sd-image.nix>
  ];

  #boot.kernelPackages = pkgs.linuxPackagesNanopiR4S;

  # compression takes loads of time with emulation, skip it.
  sdImage.compressImage = false;

  sdImage = {
    populateFirmwareCommands = '''';
    populateRootCommands = ''
      mkdir -p ./files/boot
      ${extlinux-conf-builder} -t 3 -c ${config.system.build.toplevel} -d ./files/boot
    '';
  };
}
