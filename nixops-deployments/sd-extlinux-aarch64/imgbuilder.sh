#!/usr/bin/env bash

OUT=out
IMG_DIR="result/sd-image"
mkdir $OUT

build_image(){
	nix-build "<nixpkgs/nixos>" --argstr system aarch64-linux -A config.system.build.sdImage -I nixos-config=$1.nix

	IMG=$(basename $IMG_DIR/*)
	cp $IMG_DIR/$IMG $OUT/$IMG
	chmod 660 $OUT/$IMG
}

copy_secret() {
	echo ""
	echo "mount image $1"
	echo ""
	echo "copy secret $2"
	echo ""
	TARGET_DIR=/tmp/sd-builder

	mkdir -p $TARGET_DIR/
	losetup --partscan --find --show $1
	mount /dev/loop0p2 $TARGET_DIR/

	cp /etc/nixos/secrets/$2.* $TARGET_DIR/

	umount /dev/loop0p2
	losetup --detach-all
	rmdir $TARGET_DIR
}

print_success() {
	DIR=$1
	NAME=$2
	echo ""
	echo "Image built successfully!"
	echo ""
	echo "Now burn the image with:"
	echo "dd if=$DIR/$NAME of=/dev/TARGET_DEV iflag=direct oflag=direct bs=16M status=progress"
	echo ""
	echo "or compress it with (might break integrity!!?!?):"
	echo "tar -c -I 'xz -9 -T0' -f nanopi-nixos-$(date --rfc-3339=date).img.xz $DIR/$NAME"
	echo ""
	echo "afterwards clean up with:"
	echo "rm -rf out/"
}
