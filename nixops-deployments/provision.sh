#! /usr/bin/env bash
set -euo pipefail

ip="192.168.43.9"
deployment="nanopi"
secrets_path="/etc/nixos/secrets"
secrets=(
	"wg-sofia-nanopi"
	"wg-vogon-nanopi"
)

for s in "${secrets[@]}"
do
	scp $secrets_path/$s* root@$ip:$secrets_path/
done

nixops deploy -d $deployment --boot

echo "now reboot the machine"
echo "nixops reboot -d $deployment"
echo "sed -i '/^$ip/d' ~/.ssh/known_hosts"
echo "and change the ip of the machine in the deployment..."
echo "then you can deploy normally with:"
echo "nixops deploy -d $deployment"
