let
  m = import ../network/machines.nix;

  ip-provision = m.provision-nixops.sofia.wg-sofia.ip;

  name = m.devpi.name;
  ip = m.devpi.home.lan.ip;
  ipW = m.devpi.home.wifi.ip;
in
{
  network.description = "Raspberry Pi Hutschi";

  devpi = { ... }: {
    networking.hostName = name;
    deployment = {
      targetHost = ipW;
      provisionSSHKey = false;
    };
    imports = [ ../hosts/devpi ];
  };
}
