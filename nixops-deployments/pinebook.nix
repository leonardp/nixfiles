let
  m = import ../network/machines.nix;
  private_keys = import ../secrets/private-keys.nix;
in
{
  network.description = "PB";

  heineken = { ... }: {
    networking.hostName = m.heineken.name;
    deployment = {
      targetHost = m.heineken.home.wifi.ip;
      # soon
      # provisionSSHKey = false;
      keys.extrasecret.text = private_keys.private;
    };
    imports = [ ../hosts/heineken ];
  };
}
