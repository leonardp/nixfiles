let
  m = import ../network/machines.nix;
  private_keys = import ../secrets/private-keys.nix;
in
{
  network.description = "Raspberry Pi HiFi/SlowFi/DNS/DHCP";

  rpi = { ... }: {
    networking.hostName = m.rpi.name;
    deployment = {
      targetHost = m.rpi.home.lan.ip;
      provisionSSHKey = false;
      # still possible??! untested...
      #keys.my-secret.text = private_keys.private;
    };
    imports = [ ../hosts/rpi ];
  };
}
