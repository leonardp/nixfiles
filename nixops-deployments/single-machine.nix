let
  private_keys = import ../secrets/private-keys.nix;
in
{
  network.description = "Single machine deployment";

  single = { config, lib, pkgs, ... }: {
    deployment = {
      targetHost = let
        ip = builtins.getEnv "IP";
      in if ip == ""
      then throw "Please set the IP environment variable to the target device."
      else ip;
      # soon
      # provisionSSHKey = false;
      keys.extrasecret.text = private_keys.private;
    };

    imports = [
      <target>
    ];
  };
}
