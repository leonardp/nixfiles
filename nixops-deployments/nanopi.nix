let
  m = import ../network/machines.nix;

  ip-provision = m.provision-nixops.sofia.wg-sofia.ip;

  name = m.nanopi.name;
  ip = m.nanopi.home.lan.ip;
in
{
  network.description = "Nanopi R4S Router/DNS/DHCP";

  nanopi = { ... }: {
    networking.hostName = name;
    deployment = {
      targetHost = ip;
      #targetHost = ip-provision;
      provisionSSHKey = false;
    };
    imports = [
      ../hosts/nanopi
    ];
  };
}
