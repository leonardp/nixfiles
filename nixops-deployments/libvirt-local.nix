let
  m = import ../network/machines.nix;
  private_keys = import ../secrets/private-keys.nix;
in
{
  network.description = "local libvirt deployment";

  runner = { ... }: {
    networking.hostName = m.runner.name;
    deployment = {
      targetHost = m.runner.home.lan.ip;
      # soon
      # provisionSSHKey = false;
      # keys.extrasecret.text = private_keys.private;
    };
    imports = [
      ../machines/qemu
      ../modules/gitlab-runner.nix
      ../profiles/baseline.nix
    ];
  };
}
