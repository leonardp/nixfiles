let
  # Cluster machines vars
  machines = import ../cluster-test/machines.nix;

  # Common node config with modules arrrgument
  node = { name, modules ? [], ... }: {
    networking.hostName = machines.${name}.name;
    deployment = { targetHost = machines.${name}.lan.ip; };
    imports = [ ../cluster-test/baseline ] ++ modules;
  } // nfs-mount;

  nfs-master = ../cluster-test/nfs-master.nix;
  nfs-mount = import ../cluster-test/nfs-mount.nix { nfs_ip = "${machines.cluster-0.lan.ip}"; };

  haproxy = import ../cluster-test/haproxy.nix {
    web_backend_1 = {name = "${machines.cluster-4.name}"; ip = "${machines.cluster-4.lan.ip}"; };
    web_backend_2 = {name = "${machines.cluster-5.name}"; ip = "${machines.cluster-5.lan.ip}"; };
  };

  nginx-static = ../cluster-test/nginx-static.nix;

  k8s-master = ../cluster-test/k8s-master.nix;
  k8s-node = ../cluster-test/k8s-node.nix;

in
{
  network.description = "Testcluster";

  cluster-0 = node { name="cluster-0"; modules=[ nfs-mount nginx-ha nfs-master ]; };
  cluster-1 = node { name="cluster-1"; modules=[ nfs-mount nginx-ha ]; };
  cluster-2 = node { name="cluster-2"; modules=[ nfs-mount nginx-static ]; };
  cluster-3 = node { name="cluster-3"; modules=[ nfs-mount nginx-static ]; };
  cluster-4 = node { name="cluster-4"; modules=[ nfs-mount nginx-static ];};
  cluster-5 = node { name="cluster-5"; modules=[ nfs-mount nginx-static ];};
}
