{ config, lib, pkgs, ... }:

with lib;

let
  cfg = config.services.meta-dhcpd;

  n = import ./networks.nix;
  m = import ./machines.nix;
  c = import ./connectivity.nix;

  machines = remove null ( flatten (forEach cfg.nets (netname:
    mapAttrsToList (name: value:
      if (hasAttrByPath [ "${name}" "${cfg.site}" "${netname}" ] m &&
        hasAttrByPath [ "${name}" "${cfg.site}" "${netname}" "mac" ] m )
      then
      {
        "ethernetAddress" = value.${cfg.site}.${netname}.mac;
        "hostName" = value.${cfg.site}.${netname}.name;
        "ipAddress" = value.${cfg.site}.${netname}.ip;
      }
      else null
    ) m
  )));

  subnets = concatStrings ( unique (forEach cfg.nets (netname:
      let
        routes = concatStringsSep ",  " (remove null (forEach c.${cfg.site}.${netname}.to (path:
          let
            subnet = attrByPath path false n;
          in
          if hasAttr "router" subnet
          then
          concatStrings (
            [
              ''${toString subnet.length}, ''
              ''${replaceStrings ["."] [", "] subnet.prefix}, ''
              ''${replaceStrings ["."] [", "] subnet.router}''
            ]
            )
          else
            null
        )));

        net = n.${cfg.site}.${netname};
        dnsServer = concatStringsSep ", " net.dnsServers;
      in
      ''
      subnet ${net.prefix} netmask ${net.netmask} {
        option routers ${net.gateway};
        option domain-name-servers ${dnsServer};
        option broadcast-address ${net.broadcast};
        range ${net.dhcpStart} ${net.dhcpStop};
        #rfc3442-classless-static-routes ${routes};
      }
      ''
  )));

  domainNames = concatStringsSep " " ( unique (forEach cfg.nets (netname:
    "${n.${cfg.site}.${netname}.domain}"
  )));

  format = pkgs.formats.json {};
in
{
  ### Interface
  options = {
    services.meta-dhcpd = {

      enable = mkEnableOption "meta-dhcpd";

      site = mkOption {
        type = types.str;
        description = "Network site defined in networks.nix";
      };

      nets = mkOption {
        type = types.listOf types.str;
        description = "Network names defined in networks.nix";
      };

      interfaces = mkOption {
        type = types.listOf types.str;
        description = "Listen Interfaces";
      };
    };
  };

  ### Implementation
  config = mkIf cfg.enable {
    #environment.etc."nixos/network/test.json".source = format.generate "zwarbl" out;
    #environment.etc."nixos/network/test.txt".source = pkgs.writeText "zra" out;
    services.dhcpd4 = {
      enable = true;
      authoritative = true;
      interfaces = cfg.interfaces;
      machines = machines;
      extraConfig = ''
        option domain-name "${domainNames}";
        option rfc3442-classless-static-routes code 121 = array of integer 8;
        option ms-classless-static-routes code 249 = array of integer 8;
        ${subnets}
      '';
    };

    # future!
    services.dhcpd6 = {
      enable = false;
    };
  };
}
