#! /usr/bin/env bash
nix-build gen-json.nix

./gen-graph.py

mv out/network.gv.png ./network-graph.png
rm -rf ./out
