#! /usr/bin/env python
import json
from graphviz import Digraph

from pprint import pprint

with open("result", 'r') as j:
    my_net = json.load(j)

# create parent graph
g = Digraph('parent_graph',
        filename='network.gv',
        graph_attr = [
            ('label', "Network Map"),
            ('splines', 'true'),
            ('overlap', 'false'),
            ('sep', '0.5'),
            ('background', "Network Map"),
            ],
        node_attr={'shape': 'record'},
        directory="./out",
        format='png',
        #engine='circo'
        engine='neato'
        )

# get network 'paths'
paths = []
for site in my_net['n'].keys():
    for net in my_net['n'][site].keys():
        paths.append((site, net))

        name = f'{site}_{net}'
        node_label = f"{net}\n{my_net['n'][site][net]['desc']}"

        if site == 'inet' and net == 'inet':
            kwarrrg = {
                    "style": "filled",
                    "fillcolor": "lightblue",
                    "shape": "tripleoctagon",
                    "pos": "0,0",
                    "color": "red"
                    }
        else:
            kwarrrg = {
                    "style": "filled",
                    "fillcolor": "lightblue",
                    "shape": "oval",
                    "color": "blue"
                    }
        g.node(name, node_label, **kwarrrg)

# add machines to their network subgraph
for n, machine in my_net['m'].items():
    for site, net in paths:
        try:
            cfg = machine[site][net]
            node_name = site + net + cfg['name']
            node_label = f"{cfg['name']}\l\n{machine['desc']}\l"
            #node_label = n
            g.node(node_name, node_label)
            g.edge(node_name, f'{site}_{net}')
        except:
            # network not defined for this machine
            pass

# connect subnets
conns = my_net['c']
for site, net in paths:
    for link in conns[site][net]['to']:
        g.edge(f'{site}_{net}', f'{link[0]}_{link[1]}')

g.view()
#u = g.unflatten(stagger=3)
#u.view()
