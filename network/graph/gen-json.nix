with import <nixpkgs> {};

let
  n = import ../networks.nix;
  m = import ../machines.nix;
  c = import ../connectivity.nix;

  merged = { "n" = n; "m" = m; "c" = c; };
  format = pkgs.formats.json {};
in runCommand "gen-json" {} ''
  cat ${format.generate "json" merged} > $out
''
