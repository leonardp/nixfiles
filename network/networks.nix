let
  wifis = import ../secrets/wifis.nix;
in
{
  inet = {
    inet = {
      desc = "This is the Internet";
    };
  };

  home = {
    lan = {
      desc = "there's no place like LAN";
      domain = "lan";

      prefix = "192.168.0.0";
      length = 24;
      cidr = "192.168.0.0/24";
      netmask = "255.255.255.0";
      broadcast = "192.168.0.255";

      gateway = "192.168.0.1";
      dnsServers = [
        "192.168.0.5"
        "192.168.0.6"
      ];

      dhcpStart = "192.168.0.111";
      dhcpStop = "192.168.0.222";
      # TODO
      # dhcpServer
      router = "192.168.0.1";
    };

    wifi = {
      desc = "UniFi WiFi Nano whatevs";
      domain = "lan";
      cfg = wifis.wifi;

      prefix = "192.168.0.0";
      length = 24;
      cidr = "192.168.0.0/24";
      netmask = "255.255.255.0";
      broadcast = "192.168.0.255";

      gateway = "192.168.0.1";
      dnsServers = [
        "192.168.0.5"
        "192.168.0.6"
      ];

      dhcpStart = "192.168.0.111";
      dhcpStop = "192.168.0.222";
      router = "192.168.0.1";
    };

    slowfi = {
      desc = "Slow Client WiFi hosted on rpi3";
      domain = "slowfi";
      cfg = wifis.slowfi;

      prefix = "192.168.1.0";
      length = 24;
      cidr = "192.168.1.0/24";
      netmask = "255.255.255.0";
      broadcast = "192.168.1.255";

      gateway = "192.168.1.1";
      dnsServers = [ "192.168.1.1" ];

      dhcpStart = "192.168.1.111";
      dhcpStop = "192.168.1.222";
      router = "192.168.0.6";
    };

    nanolan = {
      desc = "LAN behind nanopi's LAN port";
      domain = "nanolan";

      prefix = "192.168.2.0";
      length = 24;
      cidr = "192.168.2.0/24";
      netmask = "255.255.255.0";
      broadcast = "192.168.2.255";

      gateway = "192.168.2.1";
      dnsServers = [ "192.168.2.1" ];

      dhcpStart = "192.168.2.111";
      dhcpStop = "192.168.2.222";
      router = "192.168.0.5";
    };
  };

  sofia = {
    wg-sofia = let wgport = 41194; in {
      desc = "Drahtschutznetzwerk auf sofia";
      domain = "cockb.org";

      port = wgport;
      publicKey = "ei/JIlLGtqSTV1OFsHkOx7aeLrI0lzXGkgK/G3Wt52k=";
      endpoint = "116.202.235.59:41194";
      #endpoint = "116.202.235.59:${lib.toString 41194}"; # where lib?

      prefix = "192.168.43.0";
      length = 24;
      cidr = "192.168.43.0/24";
      netmask = "255.255.255.0";
      broadcast = "192.168.43.255";
      router = "192.168.43.1";
    };
    private = {
      desc = "private ipv4 subnet";
      domain = "zzz";
      prefix = "172.18.0.0";
      length = 24;
      cidr = "172.18.0.0/24";
      netmask = "255.255.255.0";
      broadcast = "172.18.0.255";
      router = "172.18.0.1";
    };
  };

  ipc = {
    vpn = {
      desc = "vpn";
      domain = "ipc.uni-tuebingen.de";
    };
  };

  tsti = {
    wg-vogo = let wgport = 51820; in {
      desc = "Drahtschutznetzwerk auf vogo";
      domain = "tsti";

      port = wgport;
      publicKey = "BcXiU2VyJ3Jwn56N18Jwv2P9g9qsCLbv6tMB9enkjE0=";
      endpoint = "134.2.220.33:51820";

      prefix = "10.1.1.0";
      length = 24;
      cidr = "10.1.1.0/24";
      netmask = "255.255.255.0";
      broadcast = "10.1.1.255";
      router = "10.1.1.1";
    };
  };
}
