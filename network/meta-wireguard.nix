{ config, lib, pkgs, ... }:

with lib;

let
  cfg = config.services.meta-wireguard;

  n = import ./networks.nix;
  m = import ./machines.nix;

  interfaces = mkMerge (forEach cfg.instances (i:
    let
      sc = n.${i.site}.${i.net};
      cc = m.${cfg.hostName}.${i.site}.${i.name};
    in
    {
      ${i.name} = {
        ips = cc.ips;
        listenPort = sc.port;
        privateKeyFile = cc.secretFile;
        peers = [
          {
            publicKey = sc.publicKey;
            allowedIPs = cc.allowedIPs;
            endpoint = sc.endpoint;
            persistentKeepalive = 25;
            presharedKeyFile = mkIf (hasAttrByPath [ "pskFile" ] cc) cc.pskFile;
          }
        ];
      };
    }
  ));

  fwPorts = flatten (forEach cfg.instances (i:
    [ n.${i.site}.${i.net}.port ]
  ));
in
{
  ### Interface
  options = {
    services.meta-wireguard = {

      enable = mkEnableOption "meta-wireguard";

      hostName = mkOption {
        type = types.str;
        description = "Host defined in networks.nix";
      };

      instances = mkOption {
        description = "Wireguard network and wireguard config defined in netowork.nix";
        example = [{
            site = "sofia";
            net = "wg-sofia";
            name = "wg-sofia";
        }];
        type = with types; listOf (submodule {
          options = {
            site = mkOption {
              type = str;
              description = "Network site defined in networks.nix";
            };
            net = mkOption {
              type = str;
              description = "Network name defined in networks.nix";
            };
            name = mkOption {
              type = str;
              description = "Client config/wg interface name defined in machines.nix";
            };
          };
        });
      };
    };
  };

  ### Implementation

  config = mkIf cfg.enable {
    # open up teh ports
    networking.firewall = { allowedUDPPorts = fwPorts; };
    networking.wireguard.interfaces = interfaces;
  };
}
