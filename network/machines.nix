(import ../cluster-test/machines.nix) // {

  nanopi = {
    desc = "Nanopi R4S";
    name = "nanopi";
    home = {
      lan = {
        name = "npwan";
        ip = "192.168.0.5";
        mac = "BE:1E:60:33:AA:2F";
        ifname = "wan";
      };
      nanolan = {
        name = "nplan";
        ip = "192.168.2.1";
        mac = "9A:30:7E:04:BE:A6";
        ifname = "lan";
      };
      slowfi = {
        name = "npslowfi";
        ip = "192.168.1.2";
        mac = "F4:F2:6D:15:77:93";
        ifname = "wlan0";
      };
    };
    sofia = {
      wg-sofia = {
        name = "nanopi.cockb.org";
        ips = [ "192.168.43.6/24" ];
        allowedIPs = [ "192.168.43.0/24" "172.18.0.0/24" ];
        secretFile = "/etc/nixos/secrets/wg-sofia-nanopi.private";
      };
    };
    tsti = {
      wg-vogo = {
        name = "nanopi-tsti";
        ips = [ "10.1.1.2/24" ];
        allowedIPs = [ "10.1.1.0/24" ];
        secretFile = "/etc/nixos/secrets/wg-vogon-nanopi.private";
        pskFile = "/etc/nixos/secrets/wg-vogon-nanopi.psk";
      };
    };
  };

  rpi = {
    desc = "Raspberry Pi 3 B+ Homeserver-chan";
    name = "rpi";
    brname = "br0";
    home = {
      lan = {
        name = "rpi";
        ip = "192.168.0.6";
        mac = "B8:27:EB:05:BD:01";
        ifname = "eth0";
      };
      slowfi = {
        name = "rpislowfi";
        ip = "192.168.1.1";
        mac = "B8:27:EB:50:E8:54";
        ifname = "wlan0";
      };
    };
  };

  adelskronen = {
    desc = "Desktop";
    name = "adelskronen";
    home = {
      lan = {
        name = "adelskronen";
        ip = "192.168.0.10";
        mac ="00:D8:61:7A:49:EC";
        ifname = "macvtap0";
      };
      wifi = {
        name = "adelskronen-wifi";
        ip = "192.168.0.50";
        mac ="84:FD:D1:C7:C7:5C";
        ifname = "wlo1";
      };
    };
    sofia = {
      wg-sofia = {
        name = "adelskronen.cockb.org";
        ips = [ "192.168.43.5/24" ];
        allowedIPs = [ "192.168.43.0/24" "172.18.0.0/24" ];
        secretFile = "/etc/nixos/secrets/wg-sofia-adelskronen.private";
      };
    };
    tsti = {
      wg-vogo = {
        name = "adelskronen-tsti";
        ips = [ "10.1.1.3/24" ];
        allowedIPs = [ "10.1.1.0/24" "134.2.192.0/24" "134.2.209.0/24" "134.2.210.0/24" ];
        secretFile = "/etc/nixos/secrets/wg-vogon-adelskronen.private";
        pskFile = "/etc/nixos/secrets/wg-vogon-adelskronen.psk";
      };
    };
    ipc = {
      vpn = {
      };
    };
  };

  heineken = {
    desc = "Pinebook Pro";
    name = "heineken";
    home = {
      lan = {
        name = "heineken-lan";
        ip = "192.168.0.11";
        #mac = "";
      };
      wifi = {
        name = "heineken";
        ip = "192.168.0.51";
        mac = "C0:84:7D:2F:71:92";
      };
    };
  };

  budweiser = {
    desc = "UniFU WiFi AP";
    name = "budweiser";
    home = {
      lan = {
        name = "budweiser";
        ip = "192.168.0.9";
        mac = "B4:FB:E4:2E:43:2A";
      };
    };
  };

  rpi-a = {
    desc = "Raspberry Pi 3 A+";
    name = "rpi-a";
    home = {
      wifi = {
        name = "rpi-a";
        ip = "192.168.0.53";
        #mac = "";
      };
    };
  };

  devpi = {
    desc = "Raspberry Pi 3 B+ Hutschiene";
    name = "devpi";
    home = {
      lan = {
        name = "devpi-lan";
        ip = "192.168.0.12";
        mac = "B8:27:EB:05:BD:01";
        ifname = "eth0";
      };
      nanolan = {
        name = "dp";
        ip = "192.168.2.5";
        mac = "B8:27:EB:05:BD:01";
        ifname = "eth0";
      };
      wifi = {
        name = "devpi";
        ip = "192.168.0.52";
        mac = "B8:27:EB:50:E8:54";
        ifname = "wlan0";
      };
    };
  };

  esp-hutschi = {
    desc = "esp hutschi";
    name = "esp-hutschi";
    home = {
      wifi = {
        name = "esp-hutschi-fb";
        ip = "192.168.0.60";
        mac = "60:01:94:86:4F:F7";
      };
      slowfi = {
        name = "esp-hutschi";
        ip = "192.168.1.10";
        mac = "60:01:94:86:4F:F7";
      };
    };
  };

  pp-hutschi = {
    desc = "pp hutschi";
    name = "pp-hutschi";
    home = {
      wifi = {
        name = "pp-hutschi-fb";
        ip = "192.168.0.61";
        mac = "A4:CF:12:D7:94:AC";
      };
      slowfi = {
        name = "pp-hutschi";
        ip = "192.168.1.11";
        mac = "A4:CF:12:D7:94:AC";
      };
    };
  };

  pp-stela = {
    desc = "pp stela";
    name = "pp-stela";
    home = {
      wifi = {
        name = "pp-stela-fb";
        ip = "192.168.0.62";
        mac = "A4:CF:12:D7:8E:92";
      };
      slowfi = {
        name = "pp-stela";
        ip = "192.168.1.12";
        mac = "A4:CF:12:D7:8E:92";
      };
    };
  };

  runner = {
    desc = "gitlab runner VM";
    name = "runner";
    home = {
      lan = {
        name = "runner";
        ifname = "enp1s0";
        ip = "192.168.0.99";
        mac = "52:54:00:6e:56:24";
      };
    };
  };

  provision-nixops = {
    desc = "...";
    name = "pn";
    sofia = {
      wg-sofia = {
        name = "pn.cockb.org";
        ip = "192.168.43.9";
        ips = [ "192.168.43.9/24" ];
        allowedIPs = [ "192.168.43.0/24" ];
        secretFile = "/wg-sofia-provision-nixops.private";
      };
    };
  };
}
