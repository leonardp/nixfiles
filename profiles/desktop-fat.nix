{ config, pkgs, ... }:
{
  imports = [
    ./baseline.nix
    ./gnome.nix
    ./bluetooth.nix
    ./printing.nix
    ./sound.nix

    ./packages/basics.nix
    ./packages/desktop.nix
    ./packages/desktop-plus.nix
    ./packages/dev.nix
    ./packages/dev-electronix.nix
    ./packages/dev-python.nix
  ];
}
