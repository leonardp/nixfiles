{ config, pkgs, ... }:
let
  n = import ../../network/networks.nix;
  m = import ../../network/machines.nix;
  name = "provision-nixops";
  wgS = "wg-sofia";
in
{
  imports = [
    ./headless.nix
    ../network/meta-wireguard.nix
  ];

  services.meta-wireguard = {
    enable = true;
    hostName = config.networking.hostName;
    instances = [
      {
        site = "sofia";
        net = wgS;
        name = wgS;
      }
    ];
  };

  networking.interfaces."${wgS}".mtu = 1390;

  networking.useDHCP = true;

  networking.firewall.enable = false;
}
