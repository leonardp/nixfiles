{ config, pkgs, ... }:
{
  imports = [
    ./baseline.nix
    ./packages/basics.nix
    ./packages/nvim.nix
  ];
}
