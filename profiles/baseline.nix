{ options, config, pkgs, lib, ... }:
{
  nix.nixPath =
    # Prepend default nixPath values.
    options.nix.nixPath.default ++
    # Append nixpkgs-overlays.
    [ "nixpkgs-overlays=/etc/nixos/modules/overlays-compat/" ]
  ;
  nixpkgs.config = {
    # Allow proprietary packages
    allowUnfree = true;
    # allowBroken = true;

    # Create an alias for the unstable channel
    packageOverrides = pkgs: {
      unstable = import <nixpkgs-unstable> {
        # pass the nixpkgs config to the unstable alias
        # to ensure `allowUnfree = true;` is propagated:
        config = config.nixpkgs.config;
      };
    };
  };

  imports = [
    ../overlays
    ../users
    ../modules/ssh.nix
    ../modules/ntp.nix
    ../modules/resolved.nix
  ];

  systemd.tmpfiles.rules = [
    "d /etc/nixos/secrets 600 root root"
  ];

  # mount tmpfs on /tmp -> not good for build hosts
  boot.tmpOnTmpfs = lib.mkDefault true;

  #security.apparmor.enable = false;

  # Select internationalisation properties.
  console = {
    font = "Lat2-Terminus16";
    # keyMap = "de"; # per host setting
  };
  i18n = {
    defaultLocale = "en_US.UTF-8";
  };

  # Set your time zone.
  time.timeZone = "Europe/Amsterdam";

  # copy the system configuration into nix-store
  system.copySystemConfiguration = true;

  programs.bash = {
    enableCompletion = true;
    interactiveShellInit  =  ''
      HISTCONTROL=erasedups:ignorespace
      HISTSIZE=-1
      HISTFILESIZE=-1
    '';
  };

  system.stateVersion = "21.11"; # Did you read the comment?
}
