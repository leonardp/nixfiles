{ pkgs, ... }:

{
  imports = [
    ./nvim.nix
  ];

  # basic packages
  environment.systemPackages = with pkgs; [
    bc
    pv
    file
    htop
    wget
    curl
    ag
    fd
    socat
    graphviz
    mscgen
    dnsutils
    killall
    iotop
    iftop
    telnet
    whois
    lsof
    xz
    lz4
    zip
    unzip
    rsync
    tmux
    tree
    usbutils
    nfs-utils
    nmap
    #netcat
    netcat-gnu
    tree
    bmon
    unstable.nix

    fzf
  ];

  programs.bash.interactiveShellInit = ''
    if command -v fzf-share >/dev/null; then
      source "$(fzf-share)/key-bindings.bash"
      source "$(fzf-share)/completion.bash"
    fi
  '';
  environment.variables = {
    FZF_DEFAULT_OPTS = "--height=25% --layout=reverse --info=inline --border --padding=1";
  };
}
