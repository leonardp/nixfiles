{ config, pkgs, ... }:
{
  environment.systemPackages = with pkgs; [
    dfu-util
    adafruit-ampy # micropython dev-tool
    #esptool       # esp32 dev-tool
    picocom
    kicad
    freecad
    pulseview
    #platformio # reenable after https://github.com/NixOS/nixpkgs/pull/114014 is merged
    i2c-tools
    #esp-idf

    unstable.stlink
    unstable.openocd
  ];
  services.udev.packages = [ pkgs.stlink ];
}
