{ config, pkgs, ... }:
let
  unstable = import <nixpkgs-unstable> {};
in
{
  environment.systemPackages = with pkgs; [
    #dfu-util
    #adafruit-ampy
    #esptool
    #picocom
    i2c-tools
    #libgpiod
    #git
    (python3.withPackages(ps: with ps; [
      black # code style
      pyserial
      python-periphery
      pyzmq
      #libiio
      unstable.libgpiod
      smbus2
    ]))
  ];
}
