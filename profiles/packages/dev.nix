{ config, pkgs, ... }:
{
  environment.systemPackages = with pkgs; [
    gitAndTools.gitFull
    diffstat
    sqlitebrowser
    #unstable.nixops
    #nixops
    unstable.nixops_unstable
    cachix
    direnv
    gnumake
    gcc
    cmake
    ninja
    entr
    parallel
    jq

    linuxPackages.bcc
    linuxPackages.bpftrace

    (python3.withPackages(ps: with ps; [
      pyfiglet # yeah
      setuptools # unnamed dependency for pyfiglet (not so "yeah")
    ]))
  ];
}
