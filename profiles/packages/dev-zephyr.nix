{ config, pkgs, ... }:
let
  unstable = import <nixpkgs-unstable> {};
in
{
  environment.systemPackages = with unstable; [
    gcc-arm-embedded
    cmake
    ninja
    gperf
    python3
    ccache
    qemu
    dtc
    # For mcuboot development, we want both Rust and go.
    #go
    #openssl.dev
    #pkgconfig
    #sqlite.dev
    #cargo
    (python3.withPackages(ps: with ps; [
      pyelftools
      pyyaml
      pykwalify
      canopen
      packaging
      progress
      psutil
      anytree
      intelhex
      west

      cryptography
      intelhex
      click
      cbor
    ]))
  ];
}
