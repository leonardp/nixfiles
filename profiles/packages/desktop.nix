{ pkgs, ... }:
{
  # minimal additional desktop packages
  environment.systemPackages = with pkgs; [
    virt-viewer
    remmina
    imagemagick
    unstable.firefox
    ffmpeg-full
  ];
}
