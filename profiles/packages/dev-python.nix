{ config, pkgs, ... }:
{
  #services.zeplBroker = {
  #  enable = true;
  #  user = "leo";
  #};

  environment.systemPackages = with pkgs; [
    #
    # overlay python applications
    #
    zepl
    #zepl-broker

    (python3.withPackages(ps: with ps; [
      black # code style
      pyserial
      pyusb
      hidapi
      jinja2
      lxml
      future
      graphviz
      cryptography
      setuptools
      pyelftools
      pyparsing
      click
      requests
      twine
      wheel
      pyzmq
      numpy
      aiohttp
      psycopg2
      pandas
      ipython
      #jupyterlab
      matplotlib
      scipy
      scikit-learn
      pyqt5
      pyyaml
      pycairo
      pygame
      erdantic
      pillow
      pytesseract

      #
      # overlay python packages
      #
      aiozyre
      #gilgamesh
      #python-blosc
      #pynng

      #
      # testing
      #
      #zepl-device
      #websockets
    ]))
  ];
}
