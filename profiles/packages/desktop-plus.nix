{ pkgs, ... }:
{
  # desktop packages
  environment.systemPackages = with pkgs; [
    pandoc
    virtmanager
    thunderbird
    libreoffice
    remmina
    inkscape
    imagemagick
    audacity
    audacious

    #qt5.full
    #qtcreator
    #python-qt

    #unstable.firefox-wayland
    firefox
    unstable.ungoogled-chromium

    pam_krb5
    exiftool
    gparted
    element-desktop
    mattermost-desktop
    texlive.combined.scheme-full
    virt-viewer
    openvpn
    ffmpeg-full
  ];
}
