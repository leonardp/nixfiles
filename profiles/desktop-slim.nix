{ config, pkgs, ... }:
{
  imports = [
    ./baseline.nix
    ./gnome.nix
    ./bluetooth.nix
    ./sound.nix

    ./packages/basics.nix
    ./packages/desktop.nix
    ./packages/dev.nix
    ./packages/dev-python.nix
  ];
}
