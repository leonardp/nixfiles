{ config, pkgs, ... }:
{
  services.xserver = {
    enable = true; # enable X
    desktopManager = {
      xterm.enable = false; # disable xterm session
      gnome = {
        enable = true;
        extraGSettingsOverrides = ''
          [org.gnome.desktop.peripherals.mouse]
          middle-click-emulation=true
        '';
      };
    };
    displayManager.gdm.enable = true;
  };
  services.gvfs.enable = true;

  #services.xserver.displayManager.sddm = {
  #  enable = true;
  #  # TODO disable virtual keyboard
  #  #extraConfig = ''
  #  #  InputMethod=
  #  #'';
  #};

  # disable what we can?
  # TODO use environment.gnome3.excludePackages
  services.gnome = {
    gnome-remote-desktop.enable = false;
    chrome-gnome-shell.enable = false;
    tracker-miners.enable = false;
    tracker.enable = false;
    games.enable = false;
    #gnome-online-accounts.enable = false;
    #gnome-online-miners.enable = false;
    #evolution-data-server.enable = false;
    #gnome-user-share.enable = false;
  };

  environment.systemPackages = with pkgs; [
    #gjs
    gnome.gnome-control-center
    gnome.gnome-themes-extra
    gnomeExtensions.gsconnect
    gnomeExtensions.easyScreenCast
    gnomeExtensions.desktop-icons-neo
  ];

  #services.udev.packages = [ pkgs.android-udev-rules ];
  services.udev.extraRules = ''
    SUBSYSTEM=="usb", ATTR{idVendor}=="0a9d", ATTR{idProduct}=="ff40", MODE="0660", GROUP="uucp", ENV{ID_MTP_DEVICE}="1", SYMLINK+="libmtp"
  '';
}
