NixOS Configurations
===

channels have to be specified on the target host which is a tragedy

run as root:
```
nix-channel --add https://nixos.org/channels/nixos-21.05 nixos
nix-channel --add https://nixos.org/channels/nixpkgs-unstable nixpkgs-unstable
nix-channel --update
cd /etc/nixos
git clone https://gitlab.com/leonardp/nixfiles .
ln -s hosts/<your own host> host/
nixos-rebuild switch
```

## add secrets
```
cp -r secrets.dist secrets
chmod -R 600 secrets
```

## configure as binary cache
```
nix-store --generate-binary-cache-key binarycache.example.com cache-priv-key.pem cache-pub-key.pem
```
private -> safe place

public -> users/pubkeys.nix

## deploy single config to specific IP via nixops
```
cd nixops-deployments
nixops create ./single-machine.nix -d single-test
IP=<machine IP> nixops deploy -I target=./machines/<machine name> -d single-test
```

## nixops deployment
```
cd nixops-deployments
nixops create ./rpi.nix -d rpi-server
nixops deploy -d rpi [--exclude <name>]
```

## build provision-nixops sd-image
```
cd nixops-deployments/sd-extlinux-aarch64
./build-nanopi.sh secret wg-sofia-provision-nixops
```
