{ config, pkgs, ... }:
{
  #nixpkgs.config.allowBroken = true;

  #system.autoUpgrade = {
  #  enable = true;
  #  dates = "9:00";
  #};
  #nix.gc = {
  #  automatic = true;
  #  dates = "21:00";
  #};

  imports = [
    ./hardware-configuration.nix
    ./configuration.nix
    ./samba.nix
    #./nextcloud.nix
    ../../profiles/headless.nix
    ../../users/qb
  ];
}
