{ config, ... }:
{
  services.samba = {
    enable = true;
    securityType = "user";
    extraConfig = ''
      workgroup = WORKGROUP
      server string = cubie
      netbios name = cubie
      hosts allow = 192.168.178. localhost
      hosts deny = 0.0.0.0/0
      log level = 5
      ntlm auth = yes
      ntlm auth = ntlmv1-permitted
      min protocol = NT1
      client min protocol = NT1
      #use sendfile = yes
      #max protocol = smb2
      #guest account = nobody
      #map to guest = bad user
    '';
    shares = {
      bilder = {
        path = "/storage/bilder";
        browseable = "yes";
        writable = "yes";
        "guest ok" = "yes";
        "create mask" = "0644";
        "directory mask" = "0755";
        "force user" = "qb";
        "force group" = "users";
      };
      data = {
        path = "/storage/data";
        browseable = "yes";
        writable = "yes";
        "guest ok" = "yes";
        "create mask" = "0644";
        "directory mask" = "0755";
        "force user" = "qb";
        "force group" = "users";
      };
      media = {
        path = "/storage/media";
        browseable = "yes";
        writable = "yes";
        "guest ok" = "yes";
        "create mask" = "0644";
        "directory mask" = "0755";
        "force user" = "qb";
        "force group" = "users";
      };
    };
  };
}
