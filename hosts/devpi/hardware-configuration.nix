{ pkgs, ... }:
{
  boot = {
    #kernelPackages = pkgs.linuxPackages_latest;
    #kernelPackages = pkgs.linuxPackages_rpi3;
    kernelParams = [
      "console=ttyS1,115200n8"
    ];
    loader = {
      grub.enable = false;
      raspberryPi = {
        enable = true;
        version = 3;
        uboot = {
          enable = true;
          configurationLimit = 5;
        };
        firmwareConfig = ''
          # 'fixes' serial console
          dtoverlay=disable-bt

          dtparam=i2c_arm=on
          dtoverlay=rtc-i2c,ds1307
        '';
      };
    };
  };
}
