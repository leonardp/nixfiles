{ config, pkgs, ... }:
{
  imports = [
    /etc/nixos/machines/rpi
    /etc/nixos/profiles/headless.nix
    #../../profiles/packages/lls1-test.nix
    /etc/nixos/machines/devicetree-overlay/ds1307.nix
    ./hardware-configuration.nix
    ./network.nix
  ];

  environment.systemPackages = with pkgs; [
    dtc
    libraspberrypi
    i2c-tools
  ];
}
