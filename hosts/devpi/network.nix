{pkgs,...}:
let
  m = import ../../network/machines.nix;
  n = import ../../network/networks.nix;
  wifis = import ../../secrets/wifi.nix;
  name = "devpi";

  priIf = m.${name}.home.lan;
  priNet = n.home.lan;

  secIf = m.${name}.home.wifi;
  secNet = n.home.wifi;

  staticIf = iFace: network:
  {
    "${iFace.ifname}" = {
      mtu = 1474;
      ipv4 = {
        addresses = [
          {
            "address" = iFace.ip;
            "prefixLength" = network.length;
          }
        ];
        routes = [
          {
            address = network.prefix;
            prefixLength = network.length;
            via = network.gateway;
          }
        ];
      };
    };
  };

  dynamicIf = iFace: {
    "${iFace.ifname}" = {
      useDHCP = true;
    };
  };
in
{
  networking.hostName = name;

  networking.useDHCP = false;
  #networking.useNetworkd = true;

  boot.kernel.sysctl = {
    "net.ipv4.conf.all.forwarding" = true;
    #"net.ipv4.conf.all.arp_filter" = true;
  };

  networking.wireless = {
    enable = true;
    networks = wifis;
    # needed as workaround
    interfaces = [ secIf.ifname ];
  };

  networking = {

    defaultGateway = secNet.gateway;
    nameservers = [ ] ++ secNet.dnsServers;

    interfaces = {} //
      dynamicIf priIf //
      #staticIf wifiIf wifiNet //
      staticIf secIf secNet;
  };
}
