{
  services.mosquitto = {
    enable = true;
    #checkPasswords = true;
    allowAnonymous = true;
    host = "0.0.0.0";
    users = {
      "iot" = {
        "password" = "asdf";
        acl = [
          "topic readwrite iot/#"
          "topic readwrite hutschi/#"
          "topic readwrite cmnd/#"
          "topic readwrite tele/#"
          "topic readwrite stat/#"
        ];
      };
      "zephyr" = {
        "password" = "zephyr";
        acl = [
          "topic readwrite zephyr/#"
        ];
      };
    };
  };
}
