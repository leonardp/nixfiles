{ pkgs, ...}:
let
  n = import ../../network/networks.nix;
  m = import ../../network/machines.nix;
  name = "nanopi";
  priIf = m.${name}.home.lan;
  priNet = n.home.lan;

  secIf = m.${name}.home.nanolan;
  secNet = n.home.nanolan;

  wifiIf = m.${name}.home.slowfi;
  wifiNet = n.home.slowfi;

  staticIf = {iFace, network, ... }:
  {
    "${iFace.ifname}" = {
      mtu = 1474;
      ipv4 = {
        addresses = [
          {
            "address" = iFace.ip;
            "prefixLength" = network.length;
          }
        ];
        routes = [
          {
            address = network.prefix;
            prefixLength = network.length;
            via = network.gateway;
          }
        ];
      };
    };
  };

  dynamicIf = { iFace }: {
    "${iFace.ifname}" = {
      useDHCP = true;
    };
  };
in
{
  networking.hostName = name;

  networking.useDHCP = false;
  #networking.useNetworkd = true; # experimental

  boot.kernel.sysctl = {
    "net.ipv4.conf.all.forwarding" = true;
    #"net.ipv4.conf.all.arp_filter" = true;
  };

  imports = [
    ../../network/meta-wireguard.nix
    ../../network/meta-dhcpd.nix
    ../../network/meta-unbound.nix
  ];

  services.meta-wireguard = {
    enable = true;
    hostName = name;
    instances = [
      {
        site = "sofia";
        net = "wg-sofia";
        name = "wg-sofia";
      }
      {
        site = "tsti";
        net = "wg-vogo";
        name = "wg-vogo";
      }
    ];
  };


  services.meta-dhcpd = {
    enable = true;
    interfaces = [
      priIf.ifname
      secIf.ifname
    ];
    site = "home";
    nets = [ "lan" "wifi" "slowfi" "nanolan" ];
  };

  services.meta-unbound = {
    enable = true;
    interfaces = [
      priIf.ifname
      secIf.ifname
    ];
    site = "home";
    nets = [ "lan" "wifi" "slowfi" "nanolan" ];
  };

  #services.hostapd = {
  #  enable = true;
  #  interface = wifiIf.ifname;
  #  ssid = wifiNet.cfg.ssid;
  #  wpaPassphrase = wifiNet.cfg.psk;
  #};

  networking = {
    dhcpcd.enable = false;

    defaultGateway = priNet.gateway;
    nameservers = [ ] ++ priNet.dnsServers;

    interfaces = {
      "wg-vogo".mtu = 1390;
      "wg-sofia".mtu = 1390;
    } //
      (staticIf { iFace = priIf; network = priNet; }) //
      #(staticIf { iFace = wifiIf; network = wifiNet; }) //
      (staticIf { iFace = secIf; network = secNet; });

    #nat = {
    #  enable = true;
    #  externalInterface = priIf.ifname;
    #  externalIP = priIf.ip;
    #  internalInterfaces = [ secIf.ifname wifiIf.ifname ];
    #  internalIPs = [ secIf.ip wifiIf.ip ];
    #};
  };

  networking.firewall.enable = false;
  #networking.firewall.checkReversePath = false;
}
