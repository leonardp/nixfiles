{ config, pkgs, ... }:
{
  imports = [
    ./hardware-configuration.nix
    ./network.nix
    ./mosquitto.nix

    ../../profiles/headless.nix
    ../../machines/nanopi-r4s
  ];

  environment.systemPackages = with pkgs; [
    wakelan
  ];
}
