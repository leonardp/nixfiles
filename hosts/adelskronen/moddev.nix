{ config, pkgs, lib, ... }:
{
  imports = [
    ../../modules/phppgadmin.nix
    ../../modules/nanomq.nix
  ];
  services.phppgadmin = {
    enable = false;
    settings = {
      theme = "cappuccino";
      left_width = 300;
      extra_login_security = false;
    };
    virtualHost = { listen = [ { addr = "0.0.0.0"; port = 9090; } ]; };
    dbHosts = [
      { desc = "LOCAL"; }
      {
        desc = "LOCAL_ip";
        host = "127.0.0.1";
      }
    ];
  };
  services.nanomq = {
    enable = false;
  };
}
