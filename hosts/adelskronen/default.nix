{ config, pkgs, ... }:
{
  #nixpkgs.config.allowBroken = true;

  #system.autoUpgrade = {
  #  enable = true;
  #  dates = "9:00";
  #};
  nix = {
    package = pkgs.nixFlakes;
    extraOptions = ''
      experimental-features = nix-command flakes
    '';
    # disabled to keep aarch64 packages
    #gc = {
    #  automatic = true;
    #  dates = "21:00";
    #};
  };

  imports = [
    ./hardware-configuration.nix
    ./network.nix
    ../../profiles/desktop-fat.nix

    ./rsnapshot.nix
    ./binary-cache.nix
    ./exports.nix
    ./udev-rules.nix

    ./nginx-epd.nix

    #./steam.nix

    ../../modules/libvirt.nix
    ../../modules/docker.nix

    ../../modules/dev-yii.nix
    ../../modules/phpdev.nix
    ../../modules/yii-advanced.nix
    ../../modules/postgresql.nix
    #../../../containers/yii
    #../../../modules/lxd.nix
    #../../profiles/packages/lls1-test.nix
    #./moddev.nix
  ];
  programs.bcc.enable = true;
}
