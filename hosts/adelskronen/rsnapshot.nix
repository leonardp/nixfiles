{
  services.rsnapshot = {
    enable = true;
    cronIntervals = {
      #"hourly"  = "0 * * * *";
      "daily"   = "0 21 * * *";
      "monthly" = "0 0 1 * *";
    };
      #retain	hourly	24
    extraConfig = ''
      snapshot_root	/mnt/data/rsnapshots/

      retain	daily	7
      retain	monthly	6

      exclude	Downloads/
      exclude	__pycache__/
      exclude	linux/
      exclude	zephyr/
      exclude	nix/
      exclude	.cache/
      exclude	.npm/
      exclude	.platformio/
      exclude	.steam/
      exclude	Steam/
      exclude	.wine/


      backup	/home/	localhost/
      backup	/opt/	localhost/
      backup	/root/	localhost/
    '';
  };
}
