{ config, ... }:
{
  services.nginx = {
    enable = true;
    virtualHosts."fractalbmp" = {
      listen = [{ addr = "0.0.0.0"; port = 8000; }];
      root = "/opt/nginx-static";
    };
  };
}
