let
  m = import ../../network/machines.nix;
  n = import ../../network/networks.nix;
  name = "adelskronen";
  priIf = m.${name}.home.lan;
  priNet = n.home.lan;
  #slowfi = n.home.slowfi;
  #nanolan = n.home.nanolan;
in
{
  networking.hostName = name;

  # required for ZFS -> `head -c 8 /etc/machine-id`
  networking.hostId = "307e7c9a";

  networking.useDHCP = false;
  #networking.useNetworkd = true; # experimental

  imports = [
    ../../network/meta-wireguard.nix
    ../../network/meta-network.nix
  ];

  services.meta-wireguard = {
    enable = true;
    hostName = name;
    instances = [
      {
        site = "sofia";
        net = "wg-sofia";
        name = "wg-sofia";
      }
      {
        site = "tsti";
        net = "wg-vogo";
        name = "wg-vogo";
      }
    ];
  };

  services.meta-network = {
    enable = true;
  };

  # thank's steven
#  networking.extraHosts =
#    let
#      hostsPath = https://raw.githubusercontent.com/StevenBlack/hosts/master/hosts;
#      hostsFile = builtins.fetchurl hostsPath;
#    in
#    builtins.readFile "${hostsFile}";
#
  networking = {
    dhcpcd.enable = false;

    #search = [ "${net.home.lan.domain}" ];

    defaultGateway = priNet.gateway;
    nameservers = [ ] ++ priNet.dnsServers;

    #macvlans.wired = {
    #  mode = "bridge";
    #  interface = "enp39s0";
    #};
    #interfaces.wired = {
    #  useDHCP = true;
    #};

    interfaces.macvtap0 = {
      #useDHCP = true;
      ipv4 = {
        addresses = [
          {
            address = priIf.ip;
            prefixLength = priNet.length;
          }
        ];
        routes = [
          #{
          #  address = slowfi.prefix;
          #  prefixLength = slowfi.length;
          #  via = m.rpi.home.lan.ip;
          #}
          #{
          #  address = nanolan.prefix;
          #  prefixLength = nanolan.length;
          #  via = m.nanopi.home.lan.ip;
          #}
        ];
      };
    };
    macvlans.macvtap0 = {
      interface = "enp39s0";
      mode = "bridge";
    };
    networkmanager.unmanaged = [
      "enp39s0"
    ];
    #interfaces.macvtap0 = {
    #  ipv4 = {
    #    addresses = [
    #      {
    #        address = "192.168.0.234";
    #        prefixLength = 24;
    #      }
    #    ];
    #  };
    #};
  };

  networking.firewall.enable = true;
  #networking.firewall.checkReversePath = false;

  # Open ports in the firewall.
  networking.firewall.allowedTCPPorts = [
    22    # ssh
    80    # 80 nginx proxy for binary cache
    111   # SunRPC
    1716  # GSConnect
    2049  # NFS
    5555  # ggm-http
    8000  # http-static
    49152 # zpinger
    34057 # zyre testapp
    20048 # zyre testapp
    42639 # zyre testapp
  ];

  networking.firewall.allowedUDPPorts = [
    123   # NTP
    111   # SunRPC
    1716  # GSConnect
    2049  # NFS
    5670  # ZRE-DISC
    43090 # zyre testapp
    59607 # zyre testapp
  ];

  # gilgamesh port range
  networking.firewall.allowedTCPPortRanges = [ { from = 6000; to = 6010; } ];
}
