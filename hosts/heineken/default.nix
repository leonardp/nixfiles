{ config, pkgs, lib, ... }:
let
  pubkey = import ../../users/pubkeys.nix;
in
{
  nixpkgs.config.allowBroken = true;
  nixpkgs.config.allowUnfree = true;
  #nixpkgs.config.allowUnsupportedSystem = true;

  imports = [
    ./hardware-configuration.nix
    ./network.nix
    ./sound.nix
    ./udev-rules.nix
    ../../profiles/laptop.nix
    ../../profiles/desktop-slim.nix
  ];

  # use adelskronen as binary cache and distributed builder
  nix.binaryCaches = [ "http://adelskronen.lan" ];
  nix.binaryCachePublicKeys = [ "${pubkey.leo_bin_cache}" ];
  nix.distributedBuilds = true;
  nix.buildMachines = [ {
    hostName = "adelskronen.lan";
    systems = [ "x86_64-linux" "aarch64-linux"];
    sshUser = "root";
    sshKey = "/root/.ssh/id_ed25519";
    maxJobs = 16;
    supportedFeatures = [ "kvm" "big-parallel" ];
  }];

  environment.systemPackages = with pkgs; [
    pulseview
  ];
}
