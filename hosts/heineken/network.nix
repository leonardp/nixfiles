let
  m = import ../../network/machines.nix;
  n = import ../../network/networks.nix;
  hostname = m.heineken.name;
in
{
  networking.hostName = hostname;

  networking.wireless.enable = false;

  networking.useDHCP = false;
  #networking.useNetworkd = true; # experimental

  networking.firewall.enable = false;
}
