{ config, pkgs, lib, ... }:
{
  # Set your time zone.
  time.timeZone = "Europe/Amsterdam";

  console = { keyMap = "uk"; };

  services.xserver = {
    enable = true;
    videoDrivers = [ "panfrost" ];
    layout = "gb";
    libinput.enable = true;
  };

  hardware.opengl = {
    enable = true;
    setLdLibraryPath = true;
    package = pkgs.mesa_drivers;
  };


  boot.loader.grub.enable = false;
  boot.loader.generic-extlinux-compatible.enable = true;
  boot.consoleLogLevel = lib.mkDefault 7;
  #boot.kernelPackages = pkgs.linuxPackages_pinebookPro;
  boot.kernelPackages = pkgs.linuxPackages_pinebookpro_lts;
  boot.kernelParams = [
    "earlyprintk"
    "cma=64M"
    "earlycon=uart8250,mmio32,0xff1a0000"
    #"console=uart8250,mmio32,0xff1a0000"
    "console=ttyS2,1500000n8"
    "video=eDP-1:1920x1080@60"
    "console=tty0"
  ];
  # do not mount tmpfs on /tmp
  #boot.tmpOnTmpfs = lib.mkOverride 100 false;

  #hardware.firmware = [ pkgs.ap6256-firmware ];
  hardware.firmware = [ pkgs.pinebookpro-ap6256-firmware];
  hardware.enableRedistributableFirmware = true;


  fileSystems = {
    "/" = {
      #device = "/dev/disk/by-path/platform-fe330000.sdhci-part1";
      device = "/dev/mmcblk2p1";
      fsType = "ext4";
    };
    "/data" = {
      device = "/dev/nvme0n1p1";
      fsType = "ext4";
      options = [ "user" "users" "exec" ]; # TODO ok?
    };
  };

  swapDevices = [ { device = "/data/swapfile"; size=8192; } ];

  nix.maxJobs = lib.mkDefault 6;

  services.udev.extraHwdb = ''
    evdev:input:b0003v258Ap001E*
      KEYBOARD_KEY_700a5=brightnessdown
      KEYBOARD_KEY_700a6=brightnessup
      KEYBOARD_KEY_70066=sleep
  '';

  fileSystems."/mnt/nfsshare" = { device = "adelskronen:/"; fsType = "nfs"; options=[ "x-systemd.idle-timeout=600" "x-systemd.automount" "_netdev" ]; };
}
