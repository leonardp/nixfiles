{pkgs,...}:
{
  # no worky with uboot?
  boot.loader.raspberryPi.firmwareConfig = ''
    dtoverlay=disable-bt
  '';

  imports = [
    /etc/nixos/machines/devicetree-overlay/dacplus.nix
  ];
  #hardware.deviceTree = {
  #  enable = true;
  #  filter = "*rpi*.dtb";
  #  overlays = [ { name = "dacplus"; dtboFile = "${pkgs.device-tree_rpi.overlays}/hifiberry-dacplus.dtbo"; } ];
  #};

  fileSystems = {
    "/usbdata" = {
      device = "/dev/disk/by-uuid/3e339201-2dfb-4ed6-bb8e-5f51919690d9";
      fsType = "ext4";
      options = [ "defaults" "nofail" "x-systemd.device-timeout=10"];
    };
    "/var/lib/containers/storage" = {
      device = "/usbdata";
      options = [ "bind" ];
    };
  };
}
