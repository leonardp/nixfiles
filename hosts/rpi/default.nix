{ config, pkgs, ... }:
{
  imports = [
    ../../machines/rpi
    ../../profiles/headless.nix
    ./hardware-configuration.nix
    ./network.nix
    ./sound.nix

    ../../modules/docker.nix
    ../../modules/gitlab-runner.nix
    #./hass.nix
  ];
  environment.systemPackages = with pkgs; [
    dtc
    libraspberrypi
    i2c-tools
  ];
}
