{ pkgs, ... }:
{
  # Enable Audio Support
  sound.enable = true;
  hardware.pulseaudio = {
    enable = true;
    package = pkgs.pulseaudioFull;
  };
}
