{pkgs,...}:
let
  m = import ../../network/machines.nix;
  n = import ../../network/networks.nix;

  name = "rpi";
  priIf = m.${name}.home.lan;
  priNet = n.home.lan;

  secIf = m.${name}.home.slowfi;
  secNet = n.home.slowfi;

  staticIf = {iFace, network, ... }:
  {
    "${iFace.ifname}" = {
      mtu = 1474;
      ipv4 = {
        addresses = [
          {
            "address" = iFace.ip;
            "prefixLength" = network.length;
          }
        ];
        routes = [
          {
            address = network.prefix;
            prefixLength = network.length;
            via = network.gateway;
          }
        ];
      };
    };
  };

  dynamicIf = { iFace }: {
    "${iFace.ifname}" = {
      useDHCP = true;
    };
  };
in
{
  networking.hostName = name;

  boot.kernel.sysctl = {
    "net.ipv4.conf.all.forwarding" = true;
  };

  imports = [
    ../../network/meta-wireguard.nix
    ../../network/meta-dhcpd.nix
    ../../network/meta-unbound.nix
  ];

  services.meta-wireguard = {
    enable = false;
    hostName = name;
    instances = [
      {
        site = "sofia";
        net = "wg-sofia";
        name = "wg-sofia";
      }
    ];
  };

  services.meta-dhcpd = {
    enable = true;
    interfaces = [
      priIf.ifname
      secIf.ifname
    ];
    site = "home";
    nets = [ "lan" "wifi" "slowfi" "nanolan" ];
  };

  services.meta-unbound = {
    enable = true;
    interfaces = [
      priIf.ifname
      secIf.ifname
    ];
    site = "home";
    nets = [ "lan" "wifi" "slowfi" "nanolan" ];
  };

  systemd.services.wpa_supplicant.enable = false;
  services.hostapd = {
    enable = true;
    interface = secIf.ifname;
    ssid = secNet.cfg.ssid;
    wpaPassphrase = secNet.cfg.psk;
  };

  networking = {
    dhcpcd.enable = false;
    useDHCP = false;

    defaultGateway = priNet.gateway;
    nameservers = [ ] ++ priNet.dnsServers;

    #wireless.interfaces = [ secIf.ifname ];
    #bridges = { br0 = { interfaces = [ m.rpi.home.lan.ifname ]; }; };

    interfaces = { } //
      (staticIf { iFace = priIf; network = priNet; }) //
      #(staticIf { iFace = wifiIf; network = wifiNet; }) //
      (staticIf { iFace = secIf; network = secNet; });
      #br0 = {
      #  ipv4 = {
      #    addresses = [
      #      {"address" = machines.rpi.lan.ip; "prefixLength" = 24;}
      #    ];
      #    routes = [
      #      { address = machines.network.lan.subnet; prefixLength = 24; via = machines.network.lan.gateway; }
      #    ];
      #  };
      #};

    #nat = {
    #  enable = true;
    #  #externalInterface = machines.rpi.brname;
    #  externalInterface = priIf.ifname;
    #  externalIP = priIf.ip;
    #  internalInterfaces = [ secIf.ifname ];
    #  internalIPs = [ secIf.ip ];
    #};

    firewall.enable = false;
  };
}
