{
  fileSystems."/nfsdisk" = { device = "/dev/vdc"; fsType = "ext4"; options=[ "rw" ]; };
  fileSystems."/mnt/nfsdisk" = { device = "/nfsdisk"; options=[ "bind" ]; };

  users.groups.anon = {
      gid = 1234;
  };
  users.users.anon = {
      uid = 1234;
      group = "anon";
  };

  services.nfs.server = {
    enable = true;
    exports = ''
      /mnt/nfsdisk *(rw,fsid=0,sync,insecure,no_subtree_check,anonuid=65534,anongid=65534)
    '';
  };
}
