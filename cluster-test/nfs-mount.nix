{ nfs_ip, ...}:
{
  # rw from cluster-0
  fileSystems."/mnt/nfsshare" = { device = "${nfs_ip}:/"; fsType = "nfs"; options=[ "x-systemd.idle-timeout=600" "x-systemd.automount" "noauto" ]; };
}
