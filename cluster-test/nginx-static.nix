{ config, ... }:
{
  services.nginx = {
    enable = true;
    virtualHosts."testsite1" = {
      root = "/var/webroot/1_test";
    };
    virtualHosts."testsite2" = {
      root = "/var/webroot/2_test";
    };
  };
}
