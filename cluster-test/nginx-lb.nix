{ config, virtual_ip, node_1, node_2, ... }:
{
  services.nginx = {
    enable = true;
    virtualHosts."testsite" = {
      root = "/var/webroot/testsite";
    };
  };
}
