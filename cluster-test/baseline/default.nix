{
  imports = [
    ../../machines/qemu
    ../../profiles/baseline.nix
    ../../modules/resizefs.nix
  ];
  # readonly for serving static files
  fileSystems."/var/webroot" = { device = "/dev/vdb"; fsType = "ext4"; options=[ "ro" ]; };
}
