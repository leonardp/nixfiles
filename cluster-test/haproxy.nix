{ web_backend_1 ? {}, web_backend_2 ? {}, ... }:
{
  services.haproxy = {
    enable = true;
    config = ''

defaults
  mode                    http
  log                     global
  option                  httplog
  option                  dontlognull
  option http-server-close
  option forwardfor       except 127.0.0.0/8
  option                  redispatch
  retries                 3
  timeout http-request    10s
  timeout queue           1m
  timeout connect         10s
  timeout client          1m
  timeout server          1m
  timeout http-keep-alive 10s
  timeout check           10s
  maxconn                 3000

frontend frontend_server
  bind :80
  mode http
  default_backend backend_server

backend backend_server
  mode http
  balance roundrobin
  server ${web_backend_1.name} ${web_backend_1.ip}:80 check
  server ${web_backend_2.name} ${web_backend_2.ip}:80 check

    '';
  };
}
