{ config, pkgs, ... }:
let
  kubeMasterIP = "192.168.0.101";
  kubeMasterHostname = "cluster-1";
  kubeMasterAPIServerPort = 443;
in
{
  # resolve master hostname
  networking.extraHosts = "${kubeMasterIP} ${kubeMasterHostname}";

  # packages for administration tasks
  environment.systemPackages = with pkgs; [
    kompose
    kubectl
    kubernetes
  ];

  services.kubernetes = {
    roles = [ "master" "node" ];
    masterAddress = kubeMasterHostname;
    easyCerts = true;
    apiserver = {
      advertiseAddress = kubeMasterIP;
      securePort = kubeMasterAPIServerPort;
    };

    # use coredns
    addons.dns.enable = true;

    # needed if you use swap
    #kubelet.extraOpts = "--fail-swap-on=false";
  };
}
