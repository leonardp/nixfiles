{
  virtual = {
    desc = "virtual ip";
    name = "ha-gw";
    home = {
      lan = {
        name = "ha-gw";
        ip = "192.168.0.111";
      };
    };
  };

  cluster-0 = {
    desc = "toy cluster node 0";
    name = "cluster-0";
    home = {
      lan = {
        name = "cluster-0";
        ip = "192.168.0.100";
        mac = "52:54:00:95:bd:d7";
      };
    };
  };
  cluster-1 = {
    desc = "toy cluster node 1";
    name = "cluster-1";
    home = {
      lan = {
        name = "cluster-1";
        ip = "192.168.0.101";
        mac = "52:54:00:bd:63:83";
      };
    };
  };
  cluster-2 = {
    desc = "toy cluster node 2";
    name = "cluster-2";
    home = {
      lan = {
        name = "cluster-2";
        ip = "192.168.0.102";
        mac = "52:54:00:ae:40:94";
      };
    };
  };
  cluster-3 = {
    desc = "toy cluster node 3";
    name = "cluster-3";
    home = {
      lan = {
        name = "cluster-3";
        ip = "192.168.0.103";
        mac = "52:54:00:e1:85:1b";
      };
    };
  };
  cluster-4 = {
    desc = "toy cluster node 4";
    name = "cluster-4";
    home = {
      lan = {
        name = "cluster-4";
        ip = "192.168.0.104";
        mac = "52:54:00:7b:7b:d5";
      };
    };
  };
  cluster-5 = {
    desc = "toy cluster node 5";
    name = "cluster-5";
    home = {
      lan = {
        name = "cluster-5";
        ip = "192.168.0.105";
        mac = "52:54:00:9c:6e:9c";
      };
    };
  };
}
