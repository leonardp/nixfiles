{
  # Imports `module.nix` if present, which is expected to add to
  # the nixos configuration system.
  imports = (
    if builtins.pathExists(./modules) then [ ./modules ]
    else []
  );

  # Imports the overlay
  nixpkgs.overlays = [
    (import ./overlay.nix)
  ];
}

