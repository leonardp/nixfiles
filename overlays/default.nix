# https://gitlab.com/samueldr/nixos-configuration/-/blob/50b1c17f3f2668b18bf83edaf67fb44e9a438ba4/overlays/default.nix
{
  # Tracked overlays
  imports = [
    ./zyre
    ./nng
    ./nanomq
    ./python
    ./unbound-adblock
    ./espressif
    ./rpi
    ./phppgadmin
  ]
  # Untracked / local overlays.
  ++ (if (builtins.pathExists(./default.local.nix)) then [ ./default.local.nix ] else [])
  ;
}
