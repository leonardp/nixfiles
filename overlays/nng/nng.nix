{ stdenv
, fetchFromGitHub
, cmake
, ninja
, mbedtls
}:
stdenv.mkDerivation rec {
  pname = "nng";
  version = "1.5.2";

  src = fetchFromGitHub {
    owner = "nanomsg";
    repo = "nng";
    rev = "169221da8d53b2ca4fda76f894bee8505887a7c6";
    hash = "sha256-qbjMLpPk5FxH710Mf8AIraY0mERbaxVVhTT94W0EV+k=";
  };

  cmakeFlags = [
    "-DNNG_ENABLE_TLS=ON"
    "-DMBEDTLS_ROOT_DIR=${mbedtls}"
    "-DNNG_ELIDE_DEPRECATED=ON" # breaks test suite
    #"-DBUILD_SHARED_LIBS=ON"
  ];

  nativeBuildInputs = [ cmake ninja ];
  propagatedBuildInputs = [ mbedtls ];

  doCheck = false;
}
