{ stdenv
, lib
, fetchurl
}:
stdenv.mkDerivation rec {
  pname = "unbound-adblock";
  version = "3.9.15";

  src = fetchurl {
    url = "https://github.com/StevenBlack/hosts/archive/${version}.tar.gz";
    sha256 = "0fjbk7vyyyb20zrr65n0mmx97nav3and7v3hx1lv5r86brspby04";
  };

  installPhase = ''
    mkdir -p $out/
    cat hosts \
      | grep '^0\.0\.0\.0' \
      | awk '{print "local-zone: \""$2"\" redirect\nlocal-data: \""$2" A 0.0.0.0\""}' \
      > $out/unbound-adblock.cfg
  '';

  meta = with lib; {
    description = "Hosts file merged from several reputable sources";
    longDescription = ''
      This repository consolidates several reputable hosts files, and merges them into a unified hosts
      file with duplicates removed. A variety of tailored hosts files are provided.
    '';
    homepage = https://github.com/StevenBlack/hosts;
    license = licenses.mit;
  };
}
