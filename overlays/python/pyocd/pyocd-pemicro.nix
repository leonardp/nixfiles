{ buildPythonPackage
, fetchPypi
, python3Packages
}:
buildPythonPackage rec {
  pname = "pyocd-pemicro";
  version = "1.0.6";

  src = fetchPypi {
    inherit pname version;
    sha256 = "10rfvkp1fjvlvz4vzvbqk7n2fjh15v94nhbiq3hd6f12zyqah42m";
  };

  buildInputs = [
    python3Packages.setuptools-scm-git-archive
    python3Packages.setuptools-scm
    python3Packages.pypemicro
  ];
  #propagatedBuildInputs = [ python3Packages.setuptools-scm-git-archive ];

  doCheck = false;
}
