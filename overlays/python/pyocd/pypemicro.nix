{ buildPythonPackage
, fetchPypi
}:
buildPythonPackage rec {
  pname = "pypemicro";
  version = "0.1.7";

  src = fetchPypi {
    inherit pname version;
    sha256 = "018fcbnqa9ayk0mh9ra2m3cdp2fp2fxl45a3mjf83v7j2l75fr0v";
  };

  #buildInputs = [ cython ];
  #propagatedBuildInputs = [ cython ];

  doCheck = true;
}
