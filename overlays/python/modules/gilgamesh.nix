{config, pkgs, lib, ...}:

let
  cfg = config.services.gilgamesh;
in

with lib;

{
  options = {
    services.gilgamesh = {
      enable = mkOption {
        default = false;
        type = with types; bool;
        description = ''
          Start GILGAMESH for a user.
        '';
      };

      user = mkOption {
        default = "username";
        type = with types; uniq str;
        description = ''
          Name of the user.
        '';
      };
    };
  };

  config = mkIf cfg.enable {
    systemd.services.gilgamesh = {
      wantedBy = [ "multi-user.target" ];
      after = [ "network.target" ];
      #description = "Start GILGAMESH as user.";
      serviceConfig = {
        Type = "simple";
        User = "${cfg.user}";
        ExecStart = ''${pkgs.gilgamesh}/bin/gilgamesh'';
      };
    };
    environment.systemPackages = [ pkgs.gilgamesh ];
  };
}
