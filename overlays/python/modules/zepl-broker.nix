{config, pkgs, lib, ...}:

let
  cfg = config.services.zeplBroker;
in

with lib;

{
  options = {
    services.zeplBroker = {
      enable = mkOption {
        default = false;
        type = with types; bool;
        description = ''
          Start a zepl broker for a user.
        '';
      };

      user = mkOption {
        default = "username";
        type = with types; uniq str;
        description = ''
          Name of the user.
        '';
      };
    };
  };

  config = mkIf cfg.enable {
    systemd.services.zeplBroker = {
      wantedBy = [ "multi-user.target" ];
      after = [ "network.target" ];
      description = "Start the zepl broker of username.";
      serviceConfig = {
        Type = "simple";
        User = "${cfg.user}";
        ExecStart = ''${pkgs.zepl-broker}/bin/zepl-broker'';
      };
    };
    environment.systemPackages = [ pkgs.zepl-broker ];
  };
}
