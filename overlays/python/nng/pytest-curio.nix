{ buildPythonPackage
, fetchPypi
, pytest
, curio
}:
buildPythonPackage rec {
  pname = "pytest-curio";
  version = "1.0.1";

  src = fetchPypi {
    inherit pname version;
    hash = "sha256-o69btvCzVk/PCF2Brn5Aes0wdWNwJVr6LOIaWJuEVZk=";
  };

  propagatedBuildInputs = [ pytest curio ];

  doCheck = false;
}
