# package vs python module ?!
#{ buildPythonPackage
#, fetchPypi
#, pyzmq
#, zepl-device
#, fetchFromGitLab
#}:

#buildPythonPackage rec {

{ lib, python3Packages }:

python3Packages.buildPythonApplication rec {
  pname = "zepl";
  version = "1.0.3";

  src = python3Packages.fetchPypi {
    inherit pname version;
    sha256 = "18jl8m8v66phi3vr7xjl0z3d9vkv3z1sbl8pyy0xic9habn2rp26";
  };

  propagatedBuildInputs = with python3Packages; [ pyzmq zepl-device ];
  doCheck = false;
}
