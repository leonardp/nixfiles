# package vs python module ?!
#{ buildPythonPackage
#, fetchPypi
#, pyzmq
#, zepl-device
#, fetchFromGitLab
#}:
#buildPythonPackage rec {

{ lib, python3Packages }:

python3Packages.buildPythonApplication rec {
  pname = "zepl-broker";
  version = "1.0.0";

  src = python3Packages.fetchPypi {
    inherit pname version;
    sha256 = "1vl7p9r1f2wrsn83wp7d7gaj4mgxa0a1npak7n6hfzldxljvwzff";
  };

  propagatedBuildInputs = with python3Packages; [ pyzmq zepl-device ];

  #src = fetchFromGitLab {
  #  owner = "zepl1";
  #  repo = "zepl-broker";
  #  rev = "master";
  #  sha256 = "0ylal1r5sjrjm76yclhbsvl6yarb371w3nvb93gwjm5qac1vai3g";
  #};
  doCheck = false;
}
