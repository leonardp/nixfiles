{ buildPythonPackage
, fetchPypi
, pyzmq
, websockets
, fetchFromGitLab
}:
buildPythonPackage rec {
  pname = "zepl-device";
  version = "1.0.0";

  propagatedBuildInputs = [ pyzmq websockets ];

  #src = fetchPypi {
  #  inherit pname version;
  #  sha256 = "0da39147xpfhhr5lcswkmchhfw6c627isdn234sdhm4crn8ib997";
  #};

  src = fetchFromGitLab {
    owner = "zepl1";
    repo = "zepl-device";
    rev = "master";
    sha256 = "1zz6yplwb9mlhmycmznfpwqprlx2bk4zg3fg698prvaqkyb5mi08";
  };
  doCheck = false;
}
