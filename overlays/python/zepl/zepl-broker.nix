# package vs python module ?!
{ buildPythonPackage
, fetchPypi
, pyzmq
, zepl-device
, fetchFromGitLab
}:
buildPythonPackage rec {

#{ lib, python3Packages }:
#python3Packages.buildPythonApplication rec {
  pname = "zepl-broker";
  version = "1.0.0";

  #propagatedBuildInputs = with python3Packages; [ pyzmq zepl-device ];
  propagatedBuildInputs = [ pyzmq zepl-device ];

  #src = fetchPypi {
  ##src = python3Packages.fetchPypi {
  #  inherit pname version;
  #  sha256 = "1vl7p9r1f2wrsn83wp7d7gaj4mgxa0a1npak7n6hfzldxljvwzff";
  #};

  src = fetchFromGitLab {
    owner = "zepl1";
    repo = "zepl-broker";
    rev = "master";
    sha256 = "1zyymqa6bb2a56l0jy50q2d43clqsky52b5lim61ykpn2ig707bp";
  };
  doCheck = false;
}
