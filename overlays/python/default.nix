# https://gitlab.com/samueldr/nixos-configuration/-/blob/50b1c17f3f2668b18bf83edaf67fb44e9a438ba4/overlays/samueldr/default.nix
#
# Generic overlay pattern
# =======================
#
# Usage
# -----
#
# Add to your `configuration.nix`:
#
# ```nix
# {
#   # [...]
#   imports = [
#     # [...] Existing imports
#     ./path/to/this/overlay/
#   ]
#   # [...]
# }
# ```
#
{
  # Imports `module.nix` if present, which is expected to add to
  # the nixos configuration system.
  imports = (
    if builtins.pathExists(./modules) then [ ./modules ]
    else []
  );

  # Imports the overlay
  nixpkgs.overlays = [
    (import ./overlay.nix)
  ];
}
