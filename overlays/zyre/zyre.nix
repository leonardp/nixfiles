{ stdenv
, lib
, fetchFromGitHub
, cmake
, zeromq
, czmq
, libsodium
, asciidoc
}:
stdenv.mkDerivation rec {
  pname = "zyre";
  version = "2.0.0";

  src = fetchFromGitHub {
    owner = "zeromq";
    repo = "zyre";
    rev = "latest_release";
    sha256 = "1iwg0yqnrn8njjqv2ycifygzxrwd5g5kaszln0hy05m1c0xi44ik";
  };

  nativeBuildInputs = [ cmake asciidoc libsodium zeromq czmq ];

  enableParallelBuilding = true;

  doCheck = false; # fails all the tests (ctest)

  meta = with lib; {
    branch = "latest_release";
    homepage = https://github.com/zeromq/zyre;
    description = "A Framework for Distributed Computing";
    license = licenses.mpl20;
    platforms = platforms.all;
  };
}

