{ stdenv
, fetchgit
, cmake
, ninja
, mbedtls
}:
stdenv.mkDerivation rec {
  pname = "nanomq";
  version = "0.5.0";

  src = fetchgit {
    #owner = "nanomq";
    #repo = "nanomq";
    fetchSubmodules = true;
    #url = "https://github.com/nanomq/nanomq";
    #rev = "4dee98214a1ee6e88901af90a0d7f3b01b7807c3";
    ##rev = "${version}";
    #sha256 = "1w851wqs45gkics5cc9jkfgis4c06c33n598hxa3sg6wx1v4hknf";
    url = "https://github.com/leonardp/nanomq";
    rev = "272f5e11052b5148b3d87191d802faaf7366d6eb";
    sha256 = "1gf4nb3vssk6si1p7cj4rnf1m4x6kxfiqlzp2xhsxq98qd6qjlin";
  };

  nativeBuildInputs = [ cmake ninja ];
  propagatedBuildInputs = [ mbedtls ];

  doCheck = false;
}
